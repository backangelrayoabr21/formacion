FROM openjdk:8-jdk-alpine
COPY target/apicurso-0.0.1.jar appv2.jar
EXPOSE 8083
ENTRYPOINT ["java", "-jar", "/appv2.jar"]